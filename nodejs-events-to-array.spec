%{?nodejs_find_provides_and_requires}
%global packagename events-to-array
%global enable_tests 1
Name:		nodejs-events-to-array
Version:	1.0.2
Release:	2
Summary:	Put a bunch of emitted events in an array, for testing
License:	ISC
URL:		https://github.com/isaacs/events-to-array
Source0:	https://registry.npmjs.org/%{packagename}/-/%{packagename}-%{version}.tgz
ExclusiveArch:	%{nodejs_arches} noarch
BuildArch:	noarch
BuildRequires:       	nodejs-packaging
%if 0%{?enable_tests}
BuildRequires:       	npm(tap)
%endif
%description
Put a bunch of emitted events in an array, for testing.

%prep
%setup -q -n package

%build

%install
mkdir -p %{buildroot}%{nodejs_sitelib}/%{packagename}
cp -pr package.json *.js \
	%{buildroot}%{nodejs_sitelib}/%{packagename}
%nodejs_symlink_deps

%check
%nodejs_symlink_deps --check
%{__nodejs} -e 'require("./")'
%if 0%{?enable_tests}
%{_bindir}/tap test/*.js
%endif

%files
%{!?_licensedir:%global license %doc}
%doc *.md
%license LICENSE
%{nodejs_sitelib}/%{packagename}

%changelog
* Thu Nov 26 2020 leiju <leiju4@huawei.com> - 1.0.2-2
- delete nodejs-events-to-array.spec.old redundancy file

* Thu Aug 20 2020 Anan Fu <fuanan3@huawei.com> - 1.0.2-1
- package init
